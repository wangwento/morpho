package com.max256.morpho.common.util;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.core.JsonParser.Feature;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * JsonTreeMap更方便的解析json字符串的工具
 * 支持读不支持写
 * 依赖jackson2.x
 * 利用jackson和递归实现复杂的json解析和数据读取
 * 支持类似js的.语法
 * 主要API 
 * 	创建方法       JsonTreeMap jsonTreeMap=JsonTreeMap.parse(String jsonString);
 * 	取数据方法   Object find=jsonTreeMap.findValue(String key); 返回结果根据规则可以强转类型 参见findValue(String key)方法注释
 * @author fbf
 * @email server@max256.com
 */
public class JsonTreeMap {
	public static String ROOT_KEY="root";//根节点本身就是个数组的话 key为root + [n] n为数组下边数字  如果不是数组的话直接 root.xxx访问即可
	public static String LENGTH="length";//获得数组元素数量的常量
	private static Pattern p = Pattern.compile("(\\[[^\\]]*\\])");//提取中括号正则表达式
	
	/**
	 * 存储数据的容器
	 */
	private  Map<String, Object> map=new TreeMap<String, Object>();
	private JsonNode root=null;//解析后的根

	/**构造函数
	 * @param jsonString json字符串
	 */
	public JsonTreeMap(String jsonString) {
		super();
		//判空
		if(null==jsonString||"".equals(jsonString)||jsonString.trim().equals("")) {
			throw new RuntimeException("The parameter cannot be empty");
		}
		//判合法
		ObjectMapper  mapper = new ObjectMapper();  
		mapper.configure(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, Boolean.TRUE);
		//Feature是个枚举类，枚举出JSON可能出现的特殊字符  只是为了兼容性
		mapper.configure(Feature.ALLOW_UNQUOTED_CONTROL_CHARS, true);//允许控制字符
		mapper.configure(Feature.ALLOW_SINGLE_QUOTES, true);//允许单引号
		mapper.configure(Feature.ALLOW_COMMENTS, true);//允许注释
		mapper.configure(Feature.ALLOW_NON_NUMERIC_NUMBERS, true);//允许单引号
	    try {
	    	root = mapper.readTree(jsonString);
		} catch (Exception e) {
			throw new RuntimeException("The parameter json string  illegality");
		}  
	    flattening(root,ROOT_KEY,"");
	   
	}
	
	/**
	 * 递归处理扁平化
	 * @param paramJsonNode需要解析的json字符串
	 * @param father 仅仅用来保留外层的键并在之后进行拼接属性.数组[n] n为数字
	 * @param fatherKey fatherKey 上一级的key名字
	 * @return
	 */
	private  void flattening(JsonNode paramJsonNode, String father,String fatherKey) {

		String indexKey="";
		//先判断元素是否是数组
		if(paramJsonNode.isArray()) {
			//是数组
			if(father.equals("")) {
				//根元素
				indexKey=father+ROOT_KEY;
			}else {
				indexKey=father+fatherKey;
			}
			int countt=0;
			//递归处理数组
			Iterator<JsonNode> fields = paramJsonNode.iterator();
			map.put(indexKey+"["+LENGTH+"]", paramJsonNode.size());//数组长度
			while(fields.hasNext()) {
				JsonNode next = fields.next();
				if(next.isBoolean()) {
					//boolean类型
					map.put(indexKey+"["+countt+"]",next.asBoolean() );
				}else if(next.isBigDecimal()){
					//isBigDecimal 表示为字符串
					map.put(indexKey+"["+countt+"]",next.toString());
				}else if(next.isArray()||next.isMissingNode()||next.isObject()){
					//数组  MissingNode 对象 表示为字符串
					map.put(indexKey+"["+countt+"]",next.toString());
				}else if(next.isLong()||next.isBigInteger()){
					//long 表示为long
					map.put(indexKey+"["+countt+"]",next.asLong());
				}else if(next.isInt()||next.isIntegralNumber()){
					//int 表示为int
					map.put(indexKey+"["+countt+"]",next.asInt());
				}else if(next.isFloat()||next.isDouble()||next.isFloatingPointNumber()){
					//float double表示为double
					map.put(indexKey+"["+countt+"]",next.asDouble());
				}else if(next.isBinary()){
					//二进制比如 base64编码的数据 toString表示
					map.put(indexKey+"["+countt+"]",next.toString());
				}else if(next.isTextual()){
					//asText
					map.put(indexKey+"["+countt+"]",next.asText());
				}else {
					//兜底 toString
					map.put(indexKey+"["+countt+"]",next.toString());
				}
		    	flattening(next,indexKey+"["+countt+"]","");//递归
		    	countt=countt+1;
			}
		  
		}else {
			//不是数组
			if(father.equals("")) {
				//根元素
				indexKey=father+ROOT_KEY;
			}else {
				indexKey=father+fatherKey+".";
			}
			//递归处理
			Iterator<Entry<String, JsonNode>> fields = paramJsonNode.fields();
			while(fields.hasNext()) {
				Entry<String, JsonNode> next = fields.next();
				if(next.getValue().isBoolean()) {
					//boolean类型
					map.put(indexKey+next.getKey(),next.getValue().asBoolean() );
				}else if(next.getValue().isBigDecimal()){
					//isBigDecimal 表示为字符串
					map.put(indexKey+next.getKey(),next.getValue().toString());
				}else if(next.getValue().isArray()||next.getValue().isMissingNode()||next.getValue().isObject()){
					//数组  MissingNode 对象 表示为字符串
					map.put(indexKey+next.getKey(),next.getValue().toString());
				}else if(next.getValue().isLong()||next.getValue().isBigInteger()){
					//long 表示为long
					map.put(indexKey+next.getKey(),next.getValue().asLong());
				}else if(next.getValue().isInt()||next.getValue().isIntegralNumber()){
					//int 表示为int
					map.put(indexKey+next.getKey(),next.getValue().asInt());
				}else if(next.getValue().isFloat()||next.getValue().isDouble()||next.getValue().isFloatingPointNumber()){
					//float double表示为double
					map.put(indexKey+next.getKey(),next.getValue().asDouble());
				}else if(next.getValue().isBinary()){
					//二进制比如 base64编码的数据 toString表示
					map.put(indexKey+next.getKey(),next.getValue().toString());
				}else if(next.getValue().isTextual()){
					//asText
					map.put(indexKey+next.getKey(),next.getValue().asText());
				}else {
					//兜底 toString
					map.put(indexKey+next.getKey(),next.getValue().toString());
				}
		    	flattening(next.getValue(),indexKey,next.getKey());//递归
			}
		}	
	}

	/**查询元素
	 * @param key key为你需要查找元素的全路径类似js的.语法 
	 * 		    比如root.data.info.status  
	 *        如果对象是数组则 root.data.list[n] n为元素下标 从0开始只能为数字 
	 *        获得数组长度则 root.data.list[length] length为固定写法 
	 * @return Object 找返回到的元素  没有找到返回null 
	 * 			如果是获得数组长度时返回null说明该元素不是数组所以无法获得数组长度
	 * 			虽然返回值是Object类型但是实际的类型是具体的类型 
	 * 			规则如下 :
	 * 			json原生boolean类型->java的Boolean类型
	 * 			json原生int类型->java的Integer类型
	 * 			json原生long类型->java的Longer类型
	 * 			json原生float或者double类型->java的Double类型
	 * 			json原生数组类型->java的String类型表示json数组的字符串
	 * 			json原生对象类型->java的String类型表示json对象的字符串
	 * 			json的value用""括起来的->java的String类型
	 * 			不符合上述规则的兜底转换原则->java的String类型 调用JsonNode的toString()方法获得请根据情况自行转换
	 * 			根据以上规则如果需要实际类型 在接收变量时直接强转即可 
	 * 
	 */
	public Object findValue(String key){
		//查找参数校验
		if(key==null||key.trim().equals("")) {
			//throws
			throw new RuntimeException("Query parameter syntax errors , can not be empty ");
		}
		//去两头空格
		String keyParam=key.trim();
		if(!keyParam.startsWith(ROOT_KEY)) {
			//throws  必须以ROOT_KEY开头
			throw new RuntimeException("Query parameter syntax errors must begin with "+ROOT_KEY);
		}
		//正则匹配[]语法 进行语法检查
        List<String> list=new ArrayList<String>();
        Matcher m = p.matcher(keyParam);
        while(m.find()){
          list.add(m.group().substring(1, m.group().length()-1));
        }
        for (String item : list) {
        	try {
        		Integer.parseInt(item);
			} catch (NumberFormatException e) {
				//不是数字再判断是不是LENGTH
				if(!item.equals(LENGTH)) {
					throw new RuntimeException("Query parameter syntax errors ");
				}
			}
		}
        //返回结果
        return this.map.get(keyParam);
	}
	

	
	/**查询匹配的所有元素
	 * @param key
	 * @return 找到的元素 没有找到返回null  
	 */
	public Collection<Object> findAllValues(){
		return this.map.values();
	}

	


	/**
	 * 获得内部存储数据的TreeMap原始对象 您需要时可以获取 一般不应调用本方法！
	 * @return
	 */
	public Map<String, Object> getMap() {
		return this.map;
	}

	/**
	 * 静态方法创建实例 
	 * @param jsonString json字符串
	 * @return JsonTreeMap对象
	 */
	public static JsonTreeMap parse(String jsonString) {
		return new JsonTreeMap(jsonString);
	}
	
	
	/**
	 * 测试用例
	 * @param args
	 */
	public static void main(String[] args) {
		//String param="{\"result\":1,\"callback\":null,\"data\":{\"count\":2,\"page_count\":1,\"floor_count\":1,\"list\":[{\"sub\":[{\"CommentId\":\"8305395\",\"ArticleId\":\"73277291\",\"BlogId\":\"5106543\",\"ParentId\":\"8300356\",\"PostTime\":\"2018-08-06 09:36:44\",\"Content\":\"[reply]liyong561[\\/reply]\\nu can u up, \\u4f60\\u53d1\\u4e2a\\u6587\\u7ae0\\uff0c\\u6211\\u5b66\\u4e60\\u4e00\\u4e0b\\u5457\\u3002\",\"UserName\":\"qq_27093465\",\"Status\":\"0\",\"IP\":\"124.207.190.10\",\"IsBoleComment\":\"0\",\"PKId\":\"0\",\"Digg\":\"0\",\"Bury\":\"0\",\"SubjectType\":\"-1\",\"WeixinArticleId\":\"0\",\"ParentUserName\":\"liyong561\",\"ParentNickName\":\"liyong561\",\"Avatar\":\"https:\\/\\/avatar.csdn.net\\/B\\/6\\/7\\/3_qq_27093465.jpg\",\"NickName\":\"\\u674e\\u5b66\\u51ef\"}],\"info\":{\"CommentId\":\"8300356\",\"ArticleId\":\"73277291\",\"BlogId\":\"5106543\",\"ParentId\":\"0\",\"PostTime\":\"2018-08-03 20:07:50\",\"Content\":\"\\u5199\\u4e86\\u8fd9\\u4e48\\u957f\\uff0c\\u6ca1\\u770b\\u89c1\\u6838\\u5fc3\\u4ee3\\u7801\\uff0c\\u5dee\\u8bc4\",\"UserName\":\"liyong561\",\"Status\":\"0\",\"IP\":\"61.51.238.155\",\"IsBoleComment\":\"0\",\"PKId\":\"0\",\"Digg\":\"0\",\"Bury\":\"0\",\"SubjectType\":\"-1\",\"WeixinArticleId\":\"0\",\"Avatar\":\"https:\\/\\/avatar.csdn.net\\/2\\/4\\/2\\/3_liyong561.jpg\",\"NickName\":\"liyong561\"}}]},\"vote\":0,\"content\":\"success\"}";
		String param="\r\n" + 
				"{\r\n" + 
				"    \"car\": {\r\n" + 
				"        \"boys\": [\r\n" + 
				"            \"tom\",\r\n" + 
				"            \"jerry\",\r\n" + 
				"            \"jack\"\r\n" + 
				"        ],\r\n" + 
				"        \"myDog\": [\r\n" + 
				"            {\r\n" + 
				"                \"alive\": true,\r\n" + 
				"                \"loyal\": true,\r\n" + 
				"                \"name\": \"大师兄的dog\"\r\n" + 
				"            },\r\n" + 
				"            {\r\n" + 
				"                \"alive\": \"false\",\r\n" + 
				"                \"loyal\": false,\r\n" + 
				"                \"name\": \"大师兄的dog\"\r\n" + 
				"            }\r\n" + 
				"        ],\r\n" + 
				"        \"price\": \"182\",\r\n" + 
				"        \"sign\": \"q7\"\r\n" + 
				"    },\r\n" + 
				"    \"name\": \"55555555555555555555555555555555555555555555555555555555555555555555555555555555555555555\" \r\n" + 
				"}";
		
		JsonTreeMap jsonTreeMap = JsonTreeMap.parse(param);
		Map<String, Object> map2 = jsonTreeMap.getMap();
		//System.out.println(map2.size());
		Set<Entry<String, Object>> entrySet = map2.entrySet();
		for (Entry<String, Object> entry : entrySet) {
			System.out.println("key  :"+entry.getKey());
			System.out.println("type :"+entry.getValue().getClass().getTypeName());
			System.out.println("value:"+entry.getValue());
			
			
		}
		
		/*Object findValue = (Object) jsonTreeMap.findValue("root.name");
		System.out.println("type:"+findValue.getClass().getTypeName());
		System.out.println("value:"+findValue);*/
		Integer findValue = (Integer) jsonTreeMap.findValue("root.car.myDog1[length]");
		if(null!=findValue) {
			for (int i = 0; i < findValue; i++) {
				System.out.println(jsonTreeMap.findValue("root.car.myDog1["+i+"].alive"));
			}
		}
		
	}
	
	
	
}
